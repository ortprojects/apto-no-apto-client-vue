import Vue from "vue";
import App from "./App.vue";
import "./registerServiceWorker";
import router from "./router";
import store from "./store";

// import styles
import "normalize.css";
import "./assets/styles/typography.css";
import "./assets/styles/global.css";
import "./assets/fonts/stylesheet.css";

import vuetify from '@/plugins/vuetify'

Vue.config.productionTip = false;

//moment
Vue.use(require("vue-moment"));

new Vue({
  router,
  store,
  vuetify,
  render: (h) => h(App),
}).$mount("#app");
