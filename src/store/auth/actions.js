import { userService } from "../../services/users";
import firebase from "firebase/app";

const actions = {
  async signUp(context, user) {
    try {
      await firebase
        .auth()
        .createUserWithEmailAndPassword(user.email, user.password);
      const currentUser = firebase.auth().currentUser;
      const idToken = await currentUser.getIdToken(true); // forceRefresh 

      context.commit("SET_CURRENT_USER", currentUser)
      context.commit("SET_ACCESS_TOKEN", idToken)
      context.commit("SET_IS_AUTHENTICATED", true)
    } catch (error) {
      console.log(error);
    }
  },
  async signInWithEmailAndPassword(context, user) {
    try {
      await firebase
        .auth()
        .signInWithEmailAndPassword(user.email, user.password);

      const currentUser = firebase.auth().currentUser;
      const idToken = await currentUser.getIdToken(true); // forceRefresh 
      console.log(currentUser.metadata);
      context.commit("SET_CURRENT_USER", currentUser)
      context.commit("SET_ACCESS_TOKEN", idToken)
      context.commit("SET_IS_AUTHENTICATED", true)
    } catch (error) {
      console.log(error);
    }
  },
  async signIn(context, user) {
    try {
      const isAuth = await userService.signIn(user);
      context.commit("SET_IS_AUTH", isAuth);
    } catch (error) {
      // handle the error here
    }
  },

  async signOut(context) {
    context.commit("SET_IS_AUTH", false);
  }
};

export default actions;
